import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-payment-qrcode',
  templateUrl: './payment-qrcode.component.html',
  styleUrls: ['./payment-qrcode.component.scss']
})
export class PaymentQrcodeComponent implements OnInit {

  constructor() { }
  infoUser = [
    {
      mahs: 'A0111101010',
      hoten: 'Lê Văn Quyết',
      namhoc: '2020 - 2021',
      ky: 'Đợt 1',
      cap: 'Tiểu học - Khu đô thị Geleximco'
    }
  ];

  ngOnInit(): void {
  }

}
