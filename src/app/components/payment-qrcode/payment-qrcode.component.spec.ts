import { ComponentFixture, TestBed } from '@angular/core/testing';

import { PaymentQrcodeComponent } from './payment-qrcode.component';

describe('PaymentQrcodeComponent', () => {
  let component: PaymentQrcodeComponent;
  let fixture: ComponentFixture<PaymentQrcodeComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ PaymentQrcodeComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(PaymentQrcodeComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
